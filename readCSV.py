import csv 
import managedb
import loggingDB

def readToInsert(file, connection, cursor):
    loggingDB.write_log_debug("Opening the csv file.")
    positionImmat = 9
    with open(file) as csvfile:
        reader = csv.reader(csvfile, delimiter='|')
        compteur = 0
        loggingDB.write_log_debug("Reading the file.")
        loggingDB.write_log_debug("Inserting the lines from the csv to the database." )
        for row in reader:
            if(compteur != 0):
                #print(row[9])
                exists = managedb.checkIfImmat(connection, cursor,row[positionImmat])
                managedb.UpsertIn(connection,cursor, row, exists)
            compteur+=1
        loggingDB.write_log_debug("Done inserting." )
    print("Importation terminé.")

