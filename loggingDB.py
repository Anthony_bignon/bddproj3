import logging
import datetime

def init_log():
    now = datetime.datetime.now().replace(microsecond=0)
    filename = 'logs/'+ str(now) + '.log'
    logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', filename=filename, level=logging.DEBUG)

def write_log_debug(str):
    logging.debug(str)

def write_log_info(str):
    logging.info(str)

def write_log_warning(str):
    logging.warning(str)

def write_log_error(str):
    logging.error(str)
