import sqlite3
import loggingDB

# opens the db, if it doesn't exist, it is created
# creates a table if it doesn't exist
# nota bene : primary key doesn't autoincrement (in fact it does, but we should avoid it
# according to the doc ) use rowid instead
# https://www.sqlite.org/autoinc.html

def createTable(dbName):
    connection = openConnection(dbName)
    cursor = openCursor(connection)
    loggingDB.write_log_debug("Creating the table if it doesn't exist.")
    cursor.execute("""CREATE TABLE IF NOT EXISTS SIV (
        adress TEXT,
        carrosserie TEXT,
        categorie TEXT,
        couleur TEXT,
        cylindree TEXT,
        date_immat TEXT,
        denomination TEXT,
        energy TEXT,
        firstname VARCHAR(30),
        immat VARCHAR(10),
        marque TEXT,
        name VARCHARR(30),
        places INTEGER(3),
        poids INTEGER(6),
        puissance INTEGER(4),
        vin INTEGER(20),
        type_variante_version TEXT) """)
    closeCursor(cursor)
    closeConnection(connection)

#If it exists, return the rowid corresponding
#if it doesn't, return 0
def checkIfImmat(connection, cursor, Immatriculation):
    exists = -1
    cursor.execute("SELECT immat, rowid FROM SIV")
    immats = cursor.fetchall()
    for immat in immats:
        if(Immatriculation == immat[0]):
            exists = immat[1]
    return exists
    
def UpsertIn(connection, cursor, ligne, rowid):
    if(rowid == -1):
        insert = """INSERT INTO SIV values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"""
        cursor.execute(insert, ligne)
    else:
        ligne.append(rowid)
        #print("update :", rowid)
        update = """ UPDATE SIV SET 
        adress = ?,
        carrosserie = ?,
        categorie = ?,
        couleur = ?,
        cylindree = ?,
        date_immat = ?,
        denomination = ?,
        energy = ?,
        firstname = ?,
        immat = ?,
        marque = ?,
        name = ?,
        places = ?,
        poids = ?,
        puissance = ?,
        vin =? ,
        type_variante_version = ?
        WHERE rowid = ? """
        cursor.execute(update, ligne)

def openConnection(dbName):
    connection = sqlite3.connect(dbName)
    loggingDB.write_log_debug("Opening the connection.")
    return connection

def closeConnection(connection):
    if connection:
        loggingDB.write_log_debug("Commiting before closing the connection.")
        connection.commit()
        loggingDB.write_log_debug("Closing the connection.")
        connection.close()
    else:
        loggingDB.write_log_debug("No need to close the connection, not opened.")

def openCursor(connec):
    loggingDB.write_log_debug("Opening the cursor.")
    return connec.cursor()

def closeCursor(c):
    if c:
        loggingDB.write_log_debug("Closing the cursor.")
        c.close()
    else:
        loggingDB.write_log_debug("No need to close the cursor, not opened.")

