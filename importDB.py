import argparse
import sys
import atexit
import loggingDB
import managedb
import readCSV

# returns the string passed by arg
# we should check if it returns a .csv file
def getCSVArg():
    parser = argparse.ArgumentParser()
    parser.add_argument("csvfile", help="import the csv file in a database")
    args = parser.parse_args()
    loggingDB.write_log_info("Reading the arg passed ...")
    if(args.csvfile[len(args.csvfile) - 4:] == '.csv'):     #checks if it end with .csv
        loggingDB.write_log_debug(args.csvfile + ": Is a csv file")
        return args.csvfile
    else:
        loggingDB.write_log_warning(args.csvfile + ": Is not a csv file")
        loggingDB.write_log_warning("NO CSV FILE")
        print("ERROR: NO CSV FILE")
        sys.exit(-1)

# initialise the logging config
def init():
    loggingDB.init_log()   #creates the logging file 
    csvfile = getCSVArg()  #returns the csv arg or exit
    managedb.createTable("auto.db") #creates the table if it doesn't exist
    return csvfile

def insert():
    csvfile = init()
    connection = managedb.openConnection("auto.db")
    cursor = managedb.openCursor(connection)
    readCSV.readToInsert(csvfile, connection, cursor)
    managedb.closeCursor(cursor)
    managedb.closeConnection(connection)
    

def main():
    insert()



if __name__ == "__main__":
    main()
